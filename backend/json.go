package main

import (
	"encoding/json"
	"net/http"
)

func jsonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func marshal(w http.ResponseWriter, object interface{}) error {
	return json.NewEncoder(w).Encode(object)
}