package main

import "net/http"

type Middleware func(next http.Handler) http.Handler

type Chain struct {
	chain []Middleware
}

func newMiddleware(middleware... Middleware) Chain {
	return Chain{chain:middleware}
}

func (c Chain) Then(handler http.Handler) http.Handler {
	if handler == nil {
		handler = http.DefaultServeMux
	}

	for i := range c.chain {
		handler = c.chain[len(c.chain) - 1 - i](handler)
	}

	return handler
}

func (c Chain) ThenFunc(fn http.HandlerFunc) http.Handler {
	return c.Then(fn)
}
