package main

import (
	"bytes"
	"fmt"
	"github.com/gorilla/context"
	"io/ioutil"
	"net/http"
)

type dic map[string]interface{}

const (
	HttpMethodGet = iota
	HttpMethodPost
)

type HttpMethod int

func doGetRequest(url string, r *http.Request) ([]byte, error) {
	return doRequest(url, HttpMethodGet, nil, []byte{}, r)
}

func doPostRequest(url string, body []byte, r *http.Request) ([]byte, error) {
	return doRequest(url, HttpMethodPost, nil, body, r)
}

func doPostRequestWithHeaders(url string, headers dic, body []byte, r *http.Request) ([]byte, error) {
	return doRequest(url, HttpMethodPost, headers, body, r)
}

func doRequest(url string,
	method HttpMethod,
	headers dic,
	body []byte,
	r *http.Request) ([]byte, error) {

	client := &http.Client{}

	_method := "GET"
	if method == HttpMethodPost {
		_method = "POST"
	}

	req, err := http.NewRequest(_method, url, bytes.NewBuffer(body))

	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")

	if headers != nil {
		for s, k := range headers {
			req.Header.Set(s, k.(string))
		}
	} else {
		accessToken := context.Get(r, "AccessToken").(string)
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", accessToken))
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	bodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return bodyData, nil
}