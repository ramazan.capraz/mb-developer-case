package main

import (
	"errors"
	"fmt"
	"net/http"
)

var errorInvalidToken = errors.New("Invalid API Bearer Token.")
var errorMBAPI = errors.New("Invalid error occured on Mercedes Benz Client API")

func apiError(w http.ResponseWriter, message string, code int) {
	w.WriteHeader(code)
	_, _ = w.Write([]byte(fmt.Sprintf(`{"error":"%v"}`, message)))
}