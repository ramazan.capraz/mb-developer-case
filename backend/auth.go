package main

import (
	"github.com/gorilla/context"
	"net/http"
	"strings"
)

func authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authToken, err := extractToken(r)
		if err != nil {
			apiError(w, err.Error(), http.StatusPreconditionFailed)
			return
		}

		context.Set(r, "AccessToken", authToken)
		next.ServeHTTP(w, r)
	})
}

func extractToken(r *http.Request) (string, error) {
	header := r.Header.Get("Authorization")
	authFields := strings.Split(header, " ")

	if len(authFields) != 2 || authFields[0] != "Bearer" {
		return "", errorInvalidToken
	}
	return authFields[1], nil
}
