package main

import (
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

const MaxServerTimeout time.Duration = 30

var server *http.Server

func createServer() {
	cors := handlers.CORS(
		handlers.AllowCredentials(),
		handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}),
		handlers.AllowedHeaders([]string{"authorization", "content-type"}),
	)

	router := mux.NewRouter()

	registerRoutes(router)

	server = &http.Server{
		Handler:      cors(router),
		ReadTimeout:  time.Second * MaxServerTimeout,
		WriteTimeout: time.Second * MaxServerTimeout,
		Addr:         fmt.Sprintf("%v:%v", os.Getenv("ServerHost"), os.Getenv("ServerPort")),
	}
}

func registerRoutes(router *mux.Router) {
	router.Handle("/auth", newMiddleware(jsonMiddleware).ThenFunc(authInfoHandler)).Methods("GET")
	router.Handle("/auth/complete", newMiddleware(jsonMiddleware).ThenFunc(authCompleteHandler)).Methods("GET")

	router.Handle("/vehicle/door-status", newMiddleware(jsonMiddleware, authMiddleware).ThenFunc(vehicleDoorStatusHandler)).Methods("GET")
	router.Handle("/vehicle/door-lock", newMiddleware(jsonMiddleware, authMiddleware).ThenFunc(setVehicleDoorLockHandler)).Methods("POST")
}

func main() {
	_ = os.Setenv("ServerHost", "localhost")
	_ = os.Setenv("ServerPort", "8899")
	_ = os.Setenv("MBAuthClientId", "6d63d0b6-8156-43b8-94fd-0ab95aab3941")
	_ = os.Setenv("MBAuthClientSecret", "nczDmchQaNUqRgvqRLyhYtsKzUhoEACTeYpVGMqOLewMGnQbcTuLlOzgcXZjzmoh")
	_ = os.Setenv("MBAuthScope", "mb:vehicle:status:general mb:user:pool:reader")
	_ = os.Setenv("MBAuthState", "MB_CASE")
	_ = os.Setenv("MBVehicleId", "22618AA592F3C72678")
	_ = os.Setenv("MBAuthRedirectUri", "http://localhost:8081/callback")

	createServer()

	log.Printf("Server is listening on port %v:%v...", os.Getenv("ServerHost"), os.Getenv("ServerPort"))

	if i, _ := strconv.ParseBool(os.Getenv("UseHTTPS")); !i {
		if err := server.ListenAndServe(); err != nil {
			log.Fatalf("server can't be created: %v", err)
		}
	} else {
		if err := server.ListenAndServeTLS(os.Getenv("SSLCertPath"), os.Getenv("SSLKeyPath")); err != nil {
			log.Fatalf("server can't be created: %v", err)
		}
	}
}
