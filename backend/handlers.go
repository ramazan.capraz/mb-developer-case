package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func authInfoHandler(w http.ResponseWriter, r *http.Request) {
	callbackUrl := os.Getenv("MBAuthRedirectUri")
	baseUrl := "https://id.mercedes-benz.com/as/authorization.oauth2?response_type=code&client_id=%s&redirect_uri=%s&scope=%s&state=%s"
	url := fmt.Sprintf(baseUrl,
		os.Getenv("MBAuthClientId"),
		callbackUrl,
		os.Getenv("MBAuthScope"),
		os.Getenv("MBAuthState"))

	_, _ = w.Write([]byte(url))
}

func authCompleteHandler(w http.ResponseWriter, r *http.Request) {
	callbackUrl := os.Getenv("MBAuthRedirectUri")
	code := r.URL.Query().Get("code")
	state := r.URL.Query().Get("state")

	if state != os.Getenv("MBAuthState") {
		apiError(w, errorMBAPI.Error(), http.StatusForbidden)
		return
	}

	authHeaderValue := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", os.Getenv("MBAuthClientId"), os.Getenv("MBAuthClientSecret"))))

	headers := dic{
		"Authorization": fmt.Sprintf("Basic %s", authHeaderValue),
		"Content-Type": "application/x-www-form-urlencoded",
	}
	url := "https://id.mercedes-benz.com/as/token.oauth2"
	body := []byte(fmt.Sprintf("grant_type=authorization_code&code=%s&redirect_uri=%s", code, callbackUrl))

	respData, err := doPostRequestWithHeaders(url, headers, body, r)
	if err != nil {
		apiError(w, errorMBAPI.Error(), http.StatusBadRequest)
		return
	}

	_, _ = w.Write(respData)
}

func vehicleDoorStatusHandler(w http.ResponseWriter, r *http.Request) {
	vehicleId := os.Getenv("MBVehicleId")

	url := fmt.Sprintf("https://api.mercedes-benz.com/experimental/connectedvehicle/v2/vehicles/%s/doors", vehicleId)

	respData, err := doGetRequest(url, r)
	if err != nil {
		apiError(w, errorMBAPI.Error(), http.StatusBadRequest)
		return
	}

	_, _ = w.Write(respData)
}

func setVehicleDoorLockHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		apiError(w, err.Error(), http.StatusUnprocessableEntity)
		return
	}

	vehicleId := os.Getenv("MBVehicleId")
	
	url := fmt.Sprintf("https://api.mercedes-benz.com/experimental/connectedvehicle/v2/vehicles/%s/doors", vehicleId)

	respData, err := doPostRequest(url, body, r)
	if err != nil {
		apiError(w, errorMBAPI.Error(), http.StatusBadRequest)
		return
	}

	_, _ = w.Write(respData)
}
