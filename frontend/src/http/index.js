import axios from "axios";
import {TokenService} from "../services";

const apiUrl = "http://localhost:8899";

const http = {
    instance: null,
    authInterceptor: null,
    authTrials: 0,
    init: function () {
        this.instance = axios.create({
            baseURL: apiUrl,
            json: true
        })
    },
    setHeader: function () {
        this.instance.defaults.headers.common.Authorization = `Bearer ${TokenService.getAccessToken()}`
    },
    clearHeader: function () {
        this.instance.defaults.headers.common = {}
    }

};

export default http;
