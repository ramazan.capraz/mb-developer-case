import http from "../../http";

export default {
    getAuthenticationUrl: async () => {
        let url = `/auth`
        return await http.instance.get(url)
    },
    getToken: async (code, state) => {
        let url = `/auth/complete?code=${code}&state=${state}`
        return await http.instance.get(url)
    }
}
