import http from "../../http";

export default {
    getDoorStatus: async () => {
        let url = `/vehicle/door-status`
        return await http.instance.get(url)
    },
    setDoorLock: async (isLocked = true) => {
        let url = `/vehicle/door-lock`
        return await http.instance.post(url, { "command" : isLocked ? "LOCK" : "UNLOCK" })
    }
}
