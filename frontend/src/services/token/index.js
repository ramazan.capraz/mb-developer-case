export default {
    setToken: (accessToken) => {
        localStorage.setItem("access_token", accessToken);
    },
    getAccessToken: () => localStorage.getItem("access_token"),
    removeToken: () => {
        localStorage.removeItem("access_token");
    }
}
