# Mercedes Benz Developer Case

Includes...
- a **backend API** written in Golang that utilizes and simplifies the usage of Mercedes Developer's API.
- a **simple frontend Vue.JS application** which uses the backend API
